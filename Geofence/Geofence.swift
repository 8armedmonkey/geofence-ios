//
//  Geofence.swift
//  Geofence
//
//  Created by Ivan Santoso on 21/04/15.
//  Copyright (c) 2015 Nakko. All rights reserved.
//

import MapKit

class Geofence: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var radius: CLLocationDistance
    var title: String
    var subtitle: String
    var logo: UIImage
    
    init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, title: String, subtitle: String, logo: UIImage!) {
        self.coordinate = coordinate
        self.radius = radius
        self.title = title
        self.subtitle = subtitle
        self.logo = logo
    }
    
}
