//
//  ViewController.swift
//  Geofence
//
//  Created by Ivan Santoso on 20/04/15.
//  Copyright (c) 2015 Nakko. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    struct Constants {
        
        static let kGeofenceId = "DemoGeofence"
        static let kDefaultLatitude = 52.331525
        static let kDefaultLongitude = 4.9658991
        static let kDefaultRadiusInMeters = 30.0
        
        static let kAnnotationGeofence = "Geofence"
        static let kAnnotationOther = "Other"
        
    }
    
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    var geofence = Geofence(
        coordinate: CLLocationCoordinate2D(
            latitude: CLLocationDegrees(Constants.kDefaultLatitude),
            longitude: CLLocationDegrees(Constants.kDefaultLongitude)
        ),
        radius: CLLocationDistance(Constants.kDefaultRadiusInMeters),
        title: "\(Constants.kGeofenceId)",
        subtitle: "\(Constants.kDefaultLatitude), \(Constants.kDefaultLongitude)",
        logo: UIImage(named: "Logo")
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeMapView()
        initializeLocationManager()
        
        switch CLLocationManager.authorizationStatus() {
        case .AuthorizedAlways:
            startUsingLocationService()
            mapView.showsUserLocation = true
            
        default:
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation is Geofence {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(Constants.kAnnotationGeofence)
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.kAnnotationGeofence)
                annotationView.image = (annotation as! Geofence).logo
                
            } else {
                annotationView.annotation = annotation
            }
            return annotationView
            
        } else {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(Constants.kAnnotationOther)
            
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: Constants.kAnnotationOther)
            } else {
                annotationView.annotation = annotation
            }
            return annotationView
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            var overlayRenderer = MKCircleRenderer(overlay: overlay)
            overlayRenderer.alpha = CGFloat(0.5)
            overlayRenderer.fillColor = UIColor.redColor()
            
            return overlayRenderer
        }
        return nil
    }
    
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager!) {
        println("Location updates paused...")
    }
    
    func locationManagerDidResumeLocationUpdates(manager: CLLocationManager!) {
        println("Location updates resumed...")
    }
    
    func locationManager(manager: CLLocationManager!, state: CLRegionState, region: CLRegion!) {
        println("Geofence state changed, state: \(state), region: \(region.identifier)")
        
        switch state {
        case .Inside: fallthrough
        case .Outside:
            handleRegionEvent(manager, state: state, region: region)
            
        default:
            break
        }
    }
    
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        println("Geofence enter, region: \(region.identifier)")
    }
    
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        println("Geofence exit, region: \(region.identifier)")
    }
    
    func locationManager(manager: CLLocationManager!, monitoringDidFailForRegion region: CLRegion!, withError error: NSError!) {
        println("Geofence monitoring failed, error: \(error.description)")
    }
    
    func locationManager(manager: CLLocationManager!, status: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
        case .AuthorizedAlways:
            startUsingLocationService()
            mapView.showsUserLocation = true
            
        default:
            break
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        for location in locations {
            if let l = location as? CLLocation {
                println("Location update, location=(\(l.coordinate.latitude), \(l.coordinate.longitude))")
            }
        }
    }
    
    private func initializeMapView() {
        mapView.delegate = self
        mapView.rotateEnabled = false
        
        mapView.region = MKCoordinateRegionMakeWithDistance(
            geofence.coordinate,
            geofence.radius,
            geofence.radius)
        
        mapView.addAnnotation(geofence)
        mapView.addOverlay(MKCircle(centerCoordinate: geofence.coordinate, radius: geofence.radius))
    }
    
    private func initializeLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    private func startUsingLocationService() {
        let region = CLCircularRegion(center: geofence.coordinate, radius: geofence.radius, identifier: Constants.kGeofenceId)
        
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringForRegion(region)
        locationManager.requestStateForRegion(region)
    }
    
    private func stopUsingLocationService() {
        locationManager.stopUpdatingLocation()
        
        for monitoredRegion in locationManager.monitoredRegions {
            if let circularRegion = monitoredRegion as? CLCircularRegion {
                locationManager.stopMonitoringForRegion(circularRegion)
            }
        }
    }
    
    private func handleRegionEvent(manager: CLLocationManager!, state: CLRegionState, region: CLRegion!) {
        var notification = UILocalNotification()
        
        if state == .Inside {
            notification.alertTitle = "Inside \(region.identifier)"
        } else {
            notification.alertTitle = "Outside \(region.identifier)"
        }
        
        notification.region = region
        notification.soundName = "Default"
        
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
}

